# Ejercicio Bootstrap - DSM

**Ejercicio de Bootstrap** de la asignatura de DSM del MUITEL

## Documentación

  - Página oficial: https://getbootstrap.com/
  - Getting started: https://getbootstrap.com/docs/5.1/getting-started/introduction/
  - Práctica: https://www.w3schools.com/bootstrap5/

## Tareas a realizar

1. Crear una home pública con:
    -  - [X] Logo
    - - [X] Idiomas
    - - [X] Input para buscador
    - - [X] Menú desplegable y responsivo
    - - [X] Carrusel fotográfico
    - - [X] Dos bloques 50-50 texto e imagen
    - - [X] 4 bloques responsivos
    - - [X] Bloque con pestañas o acordeones
    - - [X] Dos bloques responsivos con formulario de login y mapa (como imagen)
    -  - [X] Pie diferenciado con tres bloques

2. Crear una página de administración:
    -  - [X] Columna izquierda con logo y menú
    -  - [X] Breadcrumb
    -  - [X] Tablas
    -  - [X] Botones + modal
    -  - [X] Barra de progreso
    -  - [X] Popovers y toolpits

## Como Iniciar
### Opción 1
Puede abrir el archivo ["index.hmtl"](./index.html) en cualquier navegador.
### Opción 2
1. Instalar la extensión ["Live Server"](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
2. Aparecerá un icono abajo a la derecha llamado "Go Live"
3. Hacer click en el icono. Este abrirá un servidor donde quedará servida la carpeta con la web.
